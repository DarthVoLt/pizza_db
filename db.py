import sqlite3
from flask import g


def get_db():
    conn = sqlite3.connect('pizza.db')
    cur = conn.cursor()
    return conn, cur


def close_connection():
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def get_pizza():
    conn, cur = get_db()
    pizza = []

    pizza_rows = cur.execute(
        'SELECT id, title, ingredients, image FROM pizza'
    ).fetchall()

    for pizza_row in pizza_rows:
        piz = {
            "id": pizza_row[0],
            "title": pizza_row[1],
            "ingredients": pizza_row[2],
            "image": pizza_row[3]
        }
        pizza.append(piz)
    close_connection()
    return pizza


def get_pizza_name(pizza_id):
    pizza = get_pizza()
    pizza_name = []

    for piz in pizza:
        if pizza_id == piz['id']:
            pizza_name.append(piz['title'])
    return pizza_name


def get_order():
    conn, cur = get_db()
    orders = []
    pizza = []
    pizza_qty = []

    order_rows = cur.execute(
        'SELECT id, client_name, client_phone, client_address, state FROM orders'
    ).fetchall()

    for order_row in order_rows:
        order_pizza = cur.execute(
            'SELECT order_id, pizza_id, pizza_qty FROM order_pizza WHERE order_id = ?', [order_row[0]]
        ).fetchall()
        if order_pizza:
            for i in order_pizza:
                pizza_id = i[1]
                pizza_qty.append(i[2])
                pizza.append(get_pizza_name(pizza_id))
        order = {
            'id': order_row[0],
            'pizza': pizza,
            'pizza_qty': pizza_qty,
            'client_name': order_row[1],
            'client_phone': order_row[2],
            'client_address': order_row[3],
            'order_state': order_row[4]
        }
        orders.append(order)
    close_connection()
    return orders


def add_order(pizza_ids, pizza_qty, client_name, client_phone, client_address, order_state):
    conn, cur = get_db()

    cur.execute(
        '''INSERT INTO orders (client_name, client_phone, client_address, state) VALUES 
        (?, ?, ?, ?)''', [client_name, client_phone, client_address, order_state]
    )
    conn.commit()
    orders_id = cur.execute(
        'SELECT id FROM orders'
    ).fetchall()
    for p_id, p_qty in zip(pizza_ids, pizza_qty):
        cur.execute(
            'INSERT INTO order_pizza (order_id, pizza_id, pizza_qty) VALUES (?, ?, ?)', [orders_id[-1][0], p_id, int(p_qty)]
        )

    conn.commit()
    close_connection()


def change_order_state(new_state, order_id):
    conn, cur = get_db()

    cur.execute(
        'UPDATE orders SET state = ? WHERE id = ?', [new_state, order_id]
    )

    conn.commit()
    close_connection()

## Технологии

* [Flask](http://flask.pocoo.org/)
* [Bootstrap](https://getbootstrap.com)

## Инструкция

* Установка зависимостей
```
pip install -r requirements.txt
```

* Инициализация базы данных
```
python init_db.py
```

* Включение DEBUG-режима в Linux
```
export FLASK_ENV=development
```
(В случае Windows, надо заменить `export` на `set`)

* Запуск веб-сервиса
```
flask run
```

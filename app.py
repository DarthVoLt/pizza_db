from flask import Flask, render_template, request, redirect, url_for, flash
import db


app = Flask(__name__)
app.secret_key = b'\xc4^2l\xf1-\x10CY\xf7'


@app.route('/')
def index():
    pizza = db.get_pizza()
    return render_template('pizza.html', pizza=pizza)


@app.route('/order', methods=['GET', 'POST'])
def cart():
    global i
    pizza_order = []
    qty_order = []
    pizza = db.get_pizza()

    if request.method == 'POST':
        for p in pizza:
            if 'pizza_name_' + str(p['id']) in request.form:
                pizza_order.append(p['id'])
                qty_order.append(request.form['qty_' + str(p['id'])])
        if not pizza_order:
            flash('Не выбрана не одна пицца')
            return redirect(url_for('cart'))
        elif not qty_order:
            flash('Не указанно количество пицц')
            return redirect(url_for('cart'))
        db.add_order(pizza_ids=pizza_order,
                     pizza_qty=qty_order,
                     client_name=request.form['client_name'],
                     client_phone=request.form['client_phone'],
                     client_address=request.form['client_address'],
                     order_state='Оформлен')

        return redirect(url_for('index'))
    else:
        return render_template('order.html', pizza=pizza)


@app.route('/pass', methods=['GET', 'POST'])
def password():
    access = 'ihatepizza'
    if request.method == 'POST':
        word = request.form['password']
        if word == access:
            return redirect(url_for('manager'))
        else:
            flash('Не верный пароль')

            return redirect(url_for('password'))
    else:
        return render_template('pass.html')


@app.route('/manager', methods=['GET', 'POST'])
def manager():
    orders = db.get_order()

    if request.method == 'POST':
        for element in request.form:
            id_tuple = element.rpartition('_')
            element_id = int(id_tuple[2])
            if request.form[str(element)] != orders[element_id - 1]['order_state']:
                db.change_order_state(
                    new_state=request.form[str(element)],
                    order_id=element_id
                )
        return render_template('pass.html')
    else:
        return render_template('manager.html', orders=orders)

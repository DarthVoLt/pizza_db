import db

schema_sql = '''
DROP TABLE IF EXISTS pizza;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_pizza;

CREATE TABLE pizza (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT UNIQUE NOT NULL,
  ingredients TEXT UNIQUE NOT NULL,
  image TEXT UNIQUE NOT NULL
);

CREATE TABLE orders (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  client_name TEXT NOT NULL,
  client_phone TEXT NOT NULL,
  client_address TEXT NOT NULL,
  state TEXT NOT NULL
);

CREATE TABLE order_pizza(
  order_id INTEGER NOT NULL,
  pizza_id INTEGER NOT NULL,
  pizza_qty INTEGER NOT NULL
);
'''

fixtures_sql = '''
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Маргарита", "тесто, помидоры, моцарелла, базилик, оливковое масло", 'https://tiktak-delivery.ru/img/dish/dxT8HziEidb8ii02.jpg?mark=%2Fhome%2Ftiktak%2Fhtdocs%2Fpublic%2Fassets%2Findex%2Fimages%2Fwater-mark-2.png&s=d4d696a1a73ec9597267ab040b0995c3'
);
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Неаполитанская", "тесто, помидоры, моцарелла, пармезан, анчоусы, базилик", "http://www.smachno.in.ua/files/2016/nesolodka-vypichka/pica-neapolitanska/pica-neapolitanska.jpg"
);
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Каприччоза", "тесто, моцарелла, помидоры, грибы, черные и зеленые оливки, артишоки, базили", "https://www.bylena.ru/images/uploaded/600x_Pizza-Capriciosa-1256-step-7928.jpg"
);
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Прошутто", "тесто, моцарелла, ветчина, черный перец, оливковое масло", "https://just-eat.by/image/data/shops/1072/3591.jpg"
);
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Дьябола", "тесто, салями и острый калабрийский перец", "http://pizza-celentano.rovno.ua/upload/items/130-big-IMG_4218.jpg"
);
INSERT INTO pizza (title, ingredients, image) VALUES (
  "Сицилийская", "тесто, сыр пекорино, томатный соус, анчоусы", "http://photo.gurmanika.com/recipe/10-11/1/sicilijskaya-picca.jpg"
);
'''


def run():
    conn, cur = db.get_db()
    cur.executescript(schema_sql)
    cur.executescript(fixtures_sql)
    conn.commit()


if __name__ == '__main__':
    run()
